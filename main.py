from question_generator import QuestionGenerator
from text_input_box import TextInputBox
from textwrap import wrap
import pygame


class QuizGame:
    WINDOW_WIDTH = 1200
    WINDOW_HEIGHT = 800
    BG_COLOR = pygame.Color('blue')

    def __init__(self):
        pygame.init()
        pygame.font.init()
        pygame.display.set_caption('quiz-game')
        self.question_gen = QuestionGenerator()
        window_size = self.WINDOW_WIDTH, self.WINDOW_HEIGHT
        self.font = pygame.font.Font(None, 40)
        self.window = pygame.display.set_mode(window_size)
        self.window.fill(self.BG_COLOR)
        self.clock = pygame.time.Clock()
        self.text_input_box = TextInputBox(1200 // 2 - 800 // 2, 600, 800, self.font)
        self.group = pygame.sprite.Group(self.text_input_box)
        pygame.display.flip()

    def render_text(self, text: str, x, y, color=pygame.Color('white')):
        text = wrap(text, width=70)
        for i, t in enumerate(text):
            img_text = self.font.render(t, True, color)
            text_width, text_height = self.font.size(t)
            coord = x - text_width / 2, y - text_height / 2 + i * text_height
            self.window.blit(img_text, coord)

    def run(self):
        question_text = ''
        question = None
        running = True
        need_new_question = True
        while running:
            self.clock.tick(60)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    running = False

            if need_new_question:
                question = self.question_gen.get_question()
                question_text = '%d букв: %s' % (len(question.answer), question.question)
                need_new_question = False

            if self.text_input_box.is_enter_complete():
                answer = self.text_input_box.get_text()
                self.text_input_box.clear()
                if answer == question.answer:
                    need_new_question = True

            self.group.update(events)

            self.window.fill(self.BG_COLOR)
            self.render_text(question_text, self.WINDOW_WIDTH // 2, 200)
            self.group.draw(self.window)
            pygame.display.flip()
            pygame.display.update()
        pygame.quit()


if __name__ == '__main__':
    QuizGame().run()
