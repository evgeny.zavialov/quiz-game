#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@file: question_generator
@author: evgeniy.zavyalov 
@date: 21.01.2024
@brief:
"""

import typing
import pandas as pd
import string
import random


class QuestionGenerator:
    url = 'https://raw.githubusercontent.com/Layerex/ozhegov-dict/master/ozhegov.txt'
    Question = typing.NamedTuple('Question', [('question', str), ('answer', str)])

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            setattr(cls, 'instance', super(QuestionGenerator, cls).__new__(cls))
        return getattr(cls, 'instance')

    @staticmethod
    def filter_vocab(_row):
        _incorrect_chars = '=><+-.!' + string.ascii_lowercase + \
                           string.ascii_uppercase

        _style = _row['STYLGL']
        _definition = _row['DEF']
        _word = _row['VOCAB']

        _min_word_len = 4

        def has_incorrect(_incorrect_chars, _string):
            return any(i in _incorrect_chars for i in _string)

        if pd.isna(_definition):
            return False
        if not pd.isna(_style):
            if has_incorrect(_incorrect_chars, _style):
                return False
        if has_incorrect(_incorrect_chars, _definition):
            return False
        if has_incorrect(_incorrect_chars, _word):
            return False
        if len(_word) < _min_word_len:
            return False
        return True

    @staticmethod
    def random_generator(_rand_max: int):
        sequence = list(range(0, _rand_max))
        random.shuffle(sequence)
        for i in sequence:
            yield i

    @property
    def bank_size(self):
        return len(self.vocab)

    def __init__(self):
        _vocab = pd.read_csv(self.url, delimiter='|')
        self.vocab = _vocab[_vocab.apply(self.filter_vocab, axis=1)]
        self.rand_gen = self.random_generator(self.bank_size - 1)

    def get_question(self) -> Question:
        i = next(self.rand_gen)
        _style = self.vocab['STYLGL'].iloc[i]
        _definition = self.vocab['DEF'].iloc[i]
        _word = self.vocab['VOCAB'].iloc[i]
        _question = ''
        if not pd.isna(_style):
            _question = _style
        _question += _definition
        return self.Question(question=_question, answer=_word)
