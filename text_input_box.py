#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame


class TextInputBox(pygame.sprite.Sprite):
    def __init__(self, x, y, w, font):
        super().__init__()
        self.enter_complete = False
        self.rect = None
        self.image = None
        self.color = (255, 255, 255)
        self.backcolor = None
        self.pos = (x, y)
        self.width = w
        self.font = font
        self.text = ""
        self.render_text()

    def render_text(self):
        surf = self.font.render(self.text, True, self.color, self.backcolor)
        self.image = pygame.Surface((max(self.width, surf.get_width() + 10), surf.get_height() + 10),
                                    pygame.SRCALPHA)
        if self.backcolor:
            self.image.fill(self.backcolor)
        self.image.blit(surf, (5, 5))
        pygame.draw.rect(self.image, self.color, self.image.get_rect().inflate(-2, -2), 2)
        self.rect = self.image.get_rect(topleft=self.pos)

    def get_text(self):
        return self.text

    def clear(self):
        self.enter_complete = False
        self.text = ''
        self.render_text()

    def is_enter_complete(self):
        return self.enter_complete

    def update(self, event_list):
        for event in event_list:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                elif event.key == pygame.K_RETURN:
                    self.enter_complete = True
                elif not self.enter_complete:
                    self.text += event.unicode
                self.render_text()
